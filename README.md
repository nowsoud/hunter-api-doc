# Task Hunter api

Task Hunter REST api documentation

## Сategories
### /task/categories
summary: gets list of categories  
description: receiving a list of main categories and child categories  
produces: application/json  
#### Parameters

name: **key**  
in: query  
description: useless constant parameter  
value: f5b201dd425a9469fda211a5342cb40f  
required: true  
format: hex  
type: string  

name: **id**  
in: query  
description: parent category ID  
required: false  
format: int32  
type: integer  

#### Response 
type: array  
items: {id, name, parent_id, image}  

#### Example
request: http://test.taskhunter.pl/api/task/category?key=f5b201dd425a9469fda211a5342cb40f&id=143
  
response:
```json
[{"id":"144","name":"\u201eZ\u0142ota r\u0105czka\u201d","parent_id":"143","image":null},{"id":"145","name":"Elektryka","parent_id":"143","image":null},{"id":"146","name":"Hydraulika","parent_id":"143","image":null},{"id":"147","name":"Malowanie \u015bcian","parent_id":"143","image":null},{"id":"148","name":"Us\u0142ugi tynkarskie","parent_id":"143","image":null},{"id":"149","name":"Sk\u0142adanie mebli","parent_id":"143","image":null},{"id":"150","name":"Okna","parent_id":"143","image":null},{"id":"151","name":"Systemy Alarmowe","parent_id":"143","image":null},{"id":"152","name":"Prace elewacyjne","parent_id":"143","image":null},{"id":"153","name":"Kompleksowe us\u0142ugi budowlane","parent_id":"143","image":null},{"id":"154","name":"Inne","parent_id":"143","image":null}]
```

## My tasks
### /task/my-tasks
summary: Gets a list of tasks  
description:  Gets a list of tasks created by an authorized user  
produces: application/json  
issues: token have to be required, but it isn't, user id have NOT to be required, but it is
#### Parameters

name: **key**  
in: query  
description: useless constant parameter  
value: f5b201dd425a9469fda211a5342cb40f  
required: true  
format: hex  
type: string  

_token have to be required, but it isn't_  
name: **token**  
in: query  
description: Security token  
required: true  
format: hex  
type: string  

_user id have NOT to be required, but it is_  
name: **uid**  
in: query  
description: User ID  
required: true  
format: Int32  
type: Integer 

name: **offset**  
in: query  
description: From which element tasks will be received
required: true  
format: Int32  
type: Integer 

name: **limit**  
in: query  
description: How many tasks will be received
required: true  
format: Int32  
type: Integer 

#### Response 
type: array  

items: {id,user_id,category_id, city_id, name,description,requirements,budget,status,created_at,close_at,executor_id,workflow_id,executor_review_id,owner_review_id,pay_type,profile:{id,user_id,phone,name,city,avatar,site,address,postal_code,idently_code,name_employeer,about,hour_price,negative_review,good_review,neutral_review},category:{id,name,parent_id,image},city:{id,name }}

#### Example
request: http://test.taskhunter.pl/api/task/my-tasks?key=f5b201dd425a9469fda211a5342cb40f&uid=35&limit=3&offset=0
  
response:
```json
[{"id":"29","user_id":"35","category_id":"165","city_id":"1","name":"Zainstalowac RTV","description":"Co to jest RTV","requirements":"undefined","budget":"500","status":"1","created_at":"1521464903","close_at":"1521464903","executor_id":null,"workflow_id":null,"executor_review_id":null,"owner_review_id":null,"pay_type":"1","profile":{"id":"29","user_id":"35","phone":"577294781","name":"\u0412\u0430\u043b\u0435\u043d\u0442\u0438\u043d \u041d\u0438\u0433\u043e\u0434\u044f\u0439\u0441\u043a\u0438\u0439","city":null,"avatar":null,"site":null,"address":null,"postal_code":null,"idently_code":null,"name_employeer":null,"about":null,"hour_price":null,"negative_review":"0","good_review":"0","neutral_review":"0"},"category":{"id":"165","name":"Instalacja sprz\u0119tu RTV","parent_id":"163","image":null},"city":{"id":"1","name":"Krak\u00f3w"}},{"id":"30","user_id":"35","category_id":"136","city_id":"1","name":"\u041f\u043e\u0445\u0430\u0432\u0430\u0442\u044c","description":"\u041d\u0443\u0436\u043d\u043e \u0431\u044b","requirements":"undefined","budget":"20","status":"1","created_at":"1521465297","close_at":"1521465297","executor_id":null,"workflow_id":null,"executor_review_id":null,"owner_review_id":null,"pay_type":"1","profile":{"id":"29","user_id":"35","phone":"577294781","name":"\u0412\u0430\u043b\u0435\u043d\u0442\u0438\u043d \u041d\u0438\u0433\u043e\u0434\u044f\u0439\u0441\u043a\u0438\u0439","city":null,"avatar":null,"site":null,"address":null,"postal_code":null,"idently_code":null,"name_employeer":null,"about":null,"hour_price":null,"negative_review":"0","good_review":"0","neutral_review":"0"},"category":{"id":"136","name":"Odebra\u0107 i przewie\u017a\u0107","parent_id":"134","image":null},"city":{"id":"1","name":"Krak\u00f3w"}},{"id":"31","user_id":"35","category_id":"145","city_id":"1","name":"task1","description":"desc","requirements":"undefined","budget":"500","status":"1","created_at":"1521465886","close_at":"1521465886","executor_id":null,"workflow_id":null,"executor_review_id":null,"owner_review_id":null,"pay_type":"1","profile":{"id":"29","user_id":"35","phone":"577294781","name":"\u0412\u0430\u043b\u0435\u043d\u0442\u0438\u043d \u041d\u0438\u0433\u043e\u0434\u044f\u0439\u0441\u043a\u0438\u0439","city":null,"avatar":null,"site":null,"address":null,"postal_code":null,"idently_code":null,"name_employeer":null,"about":null,"hour_price":null,"negative_review":"0","good_review":"0","neutral_review":"0"},"category":{"id":"145","name":"Elektryka","parent_id":"143","image":null},"city":{"id":"1","name":"Krak\u00f3w"}}]
```
## My profile
### /profile/my-profile
summary: gets authorized user   
description:  gets authorized user   
produces: application/json  
issues: Response type is an array with single element  
#### Parameters

name: **key**  
in: query  
description: useless constant parameter  
value: f5b201dd425a9469fda211a5342cb40f  
required: true  
format: hex  
type: string  

name: **token**  
in: query  
description: Security token  
required: true  
format: hex  
type: string  

#### Response 
type: array
items: {id,user_id,phone,name,city,avatar,site,address,postal_code,idently_code,name_employeer,about,hour_price,negative_review,good_review,neutral_review}  

#### Example
request: http://test.taskhunter.pl/api/profile/my-profile?key=f5b201dd425a9469fda211a5342cb40f&token=5aafb8445d6cc
  
response:
```json
[{"id":"29","user_id":"35","phone":"577294781","name":"\u0412\u0430\u043b\u0435\u043d\u0442\u0438\u043d \u041d\u0438\u0433\u043e\u0434\u044f\u0439\u0441\u043a\u0438\u0439","city":null,"avatar":null,"site":null,"address":null,"postal_code":null,"idently_code":null,"name_employeer":null,"about":null,"hour_price":null,"negative_review":"0","good_review":"0","neutral_review":"0"}]
```

## Create Task
### /task/create-task
summary: Creation Task  
description: Creation new task from authorized user  
produces: UNKNOWN  
issues: reponse is not a JSON  
#### Parameters

name: **key**  
in: query  
description: useless constant parameter  
value: f5b201dd425a9469fda211a5342cb40f  
required: true  
format: hex  
type: string  

name: **token**  
in: query  
description: Security token  
required: true  
format: hex  
type: string  

name: **title**  
in: query  
description:  Task title  
required: true  
format: text 
type: String  

name: **description**  
in: query  
description:  Task description  
required: true  
format: text 
type: String  

name: **requirements**  
in: query  
description:  Task requirements  
required: true  
format: text 
type: String  

name: **budget**  
in: query  
description: Task budget  
required: true  
format: int32  
type: integer  

name: **city**  
in: query  
description: city ID  
required: true  
format: int32  
type: integer  

name: **pay_type**  
in: query  
description: pay_type ID  
required: true  
format: int32  
type: integer  

name: **category**  
in: query  
description: category ID  
required: true  
format: int32  
type: integer  

#### Response 
type: UNKNOWN  

#### Example
request: http://test.taskhunter.pl/api/task/create-task?key=f5b201dd425a9469fda211a5342cb40f&title=hk&description=hjk&requirements=undefined&budget=54&token=5aafb8445d6cc&city=1&pay_type=1&category=146
  
response:
```json
51
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details